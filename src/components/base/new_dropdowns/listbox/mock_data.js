export const mockOptions = [
  {
    value: 'prod',
    text: 'Product',
  },
  {
    value: 'ppl',
    text: 'People',
  },
  {
    value: 'fin',
    text: 'Finance',
  },
  {
    value: 'leg',
    text: 'Legal',
  },
  {
    value: 'eng',
    text: 'Engineering',
  },
  {
    value: 'sales',
    text: 'Sales',
  },
  {
    value: 'marketing',
    text: 'Marketing',
  },
  {
    value: 'acc',
    text: 'Accounting',
  },
  {
    value: 'hr',
    text: 'Human Resource Management',
  },
  {
    value: 'rnd',
    text: 'Research and Development',
  },
  {
    value: 'cust',
    text: 'Customer Service',
  },
  {
    value: 'sup',
    text: 'Support',
  },
];

export const mockGroups = [
  {
    text: 'Branches',
    options: [
      { text: 'main', value: 'main' },
      { text: 'feature-123', value: 'feature-123' },
    ],
  },
  {
    text: 'Tags',
    options: [
      { text: 'v1.0', value: 'v1.0' },
      { text: 'v2.0', value: 'v2.0' },
      { text: 'v2.1', value: 'v2.1' },
    ],
  },
];
